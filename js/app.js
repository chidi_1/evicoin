/**
 * appName - http://chidi-frontend.esy.es/
 * @version v0.1.0
 * @author bev-olga@yandex.ru
 */
// графи
function cheateChart(data_chart) {
    AmCharts.makeChart("chartdiv",
        {
            "type": "serial",
            "categoryField": "category",
            "columnSpacing": 0,
            "autoMarginOffset": 0,
            "marginBottom": 0,
            "marginLeft": 0,
            "marginRight": 0,
            "marginTop": 0,
            "colors": [
                "#3a73dc"
            ],
            "color": "#6F77B4",
            "fontSize": 13,
            "categoryAxis": {
                "axisColor": "#2D3B7A",
                "axisThickness": 0,
                "fontSize": 12,
                "gridColor": "#2d3b7a",
                "tickLength": 0,
                "titleColor": "undefined"
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "[[date]] [[month]] : [[sum]]",
                    "bullet": "round",
                    "bulletSize": 5,
                    "id": "AmGraph-1",
                    "lineThickness": 3,
                    "title": "graph 1",
                    "type": "smoothedLine",
                    "date": "date",
                    "month": "month",
                    "valueField": "sum"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "axisColor": "#2D3B7A",
                    "title": ""
                }
            ],
            "allLabels": [
                {
                    "id": "Label-1"
                }
            ],
            "balloon": {
                "borderThickness": 0,
                "color": "#3A73DC",
                "fillAlpha": 1,
                "horizontalPadding": 15,
                "verticalPadding": 11
            },
            "titles": [],
            "dataProvider": data_chart
        }
    );
}

// запуск видео
$(document).on('click', '.js--play-video', function () {
    $(this).addClass('hidden-block');
    var href = $(this).attr('href').slice(1);
    playVid(href);
    return false;
});

function playVid(href) {
    var vid = document.getElementById(href);
    vid.setAttribute('controls', 'controls');
    vid.play();
}

// параллакс кругов
function parallaxScroll() {
    var scrolled = $(window).scrollTop();
    if (scrolled > 50) {
        $('.header').addClass('scrolled')
    }
    else {
        $('.header').removeClass('scrolled')
    }
    $('.parallax-circle').css('margin-top', (0 - (scrolled * .25)) + 'px');
}

// загрузить новую капчу
function loadNewCapcha() {
    var capcha = $('.capcha');
    var url = capcha.data('url');
    var method = capcha.data('method');
    var error_container = capcha.closest('form').find('.error-message');

    $.ajax({
        type: method,
        url: url,
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);

            capcha.find('.capcha-img').attr('src', parse_data.img_url);
            capcha.find('.hidden-audio').empty().html('<audio id="audiocapcha"> ' +
                '<source src="' + parse_data.audio_url_ogg + '">' +
                '<source src="' + parse_data.audio_url_mp3 + '">' +
                '</audio>')
        },
        error: function (data) {
            error_container.text('Ошибка при отправке');

            setTimeout(function () {
                error_container.text('');
            }, 2000)
        }
    });
}

// отправить капчу на проверку
function sendCapcha() {
    var capcha = $('.inp-code');
    var url = capcha.data('url');
    var method = capcha.data('method');
    var error_container = capcha.closest('form').find('.error-message');

    $.ajax({
        type: method,
        url: url,
        data: {'code': capcha.val()},
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);

            if (parse_data.success == "false") {
                capcha.addClass('error');
                error_container.text(parse_data.error);
            }
            else {
                capcha.addClass('success');
                checkForm(capcha.closest('form'));
            }
        },
        error: function (data) {
            error_container.text('Ошибка при отправке');

            setTimeout(function () {
                error_container.text('');
            }, 2000)
        }
    });
}

// валидация почты
function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
}

$(document).ready(function () {
    parallaxScroll();

    $(window).scroll(function (e) {
        parallaxScroll();
    });

    // мобильное меню
    $('.js--open-menu').on('click', function () {
        $('.header').addClass('open');
        return false;
    });
    $('.js--close-menu').on('click', function () {
        $('.header').removeClass('open');
        return false;
    });

    // прокрутка страницы
    $('.js--page-scroll').click(function () {
        $('.header').removeClass('open');
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top - 200 + "px"
        }, {
            duration: 500
        });
        return false;
    });

    // fancybox
    $('.fancy').fancybox({
        afterLoad: function () {
            $(".fancybox-inner .scrollbar-wrap").customScrollbar();
        }
    });

    // вертикальный скролл стилизация
    if (!(device.mobile() || device.tablet())) {
        $(".tabs .scrollbar-wrap").customScrollbar();
    }

    // faq аккордеон
    if (device.mobile() || device.tablet()) {
        $('.tabs--content .scrollbar-wrap').hide();
        $('.tabs--content:nth-child(2) .justifyed').next().show();

        $('.tabs--content .justifyed').click(function () {
            if ($(this).next().is(':hidden')) {
                $('.tabs--content.active').removeClass('active').find('.scrollbar-wrap').slideUp();
                $(this).toggleClass('active').next().slideDown().closest('.tabs--content').addClass('active');
            }

            return false;
        });

        $('.team-people ul').owlCarousel({
            margin: 0,
            loop: true,
            autoWidth: false,
            autoHeight: true,
            nav: true,
            dots: false,
            navText: [,],
            autoplay: false,
            items: 1
        });
    }

    // табы
    $('ul.tabs--caption').each(function () {
        $(this).find('li').each(function (i) {
            $(this).click(function () {
                $(this).addClass('active').siblings().removeClass('active')
                    .closest('.tabs').find('.tabs--content').removeClass('active').eq(i).addClass('active');
            });
        });
    });

    $('.js--next-tab').on('click', function () {
        var link = $(this);
        var index = link.closest('.tabs--content').index();
        var caption = link.closest('.tabs').find('.tabs--caption');

        if (index < $(caption).find('li').length) {
            $(caption).find('li').eq(index).trigger('click');
        }

        return false;
    });

    // ФОРМА
    // убираем подсветку при фокусе
    $('.inp').focus(function () {
        var inp = $(this);
        inp.removeClass('error');
        if (inp.hasClass('inp-code')) {
            inp.closest('form').find('.error-message').text("");
        }
    });

    // проверка на корректно заполненное поле
    $('.inp.required').on('blur', function () {
        var inp = $(this);

        if (inp.val() == '') {
            inp.removeClass('success').addClass('error');
        }
        else {
            if (inp.hasClass('inp-mail')) {
                if (validateEmail(inp.val()) == false) {
                    inp.removeClass('success').addClass('error');
                }
                else {
                    inp.removeClass('error').addClass('success');
                }
            }
            else {
                if (inp.hasClass('inp-phone')) {
                    if ((inp.val().toString().indexOf('_') + 1)) {
                        inp.removeClass('success').addClass('error');
                    }
                }
                else {
                    inp.removeClass('error').addClass('success');
                }
            }
        }
        checkForm(inp.closest('form'));
    });

    // отправить телефон на проверку
    $('.inp-phone').mask('+7 (999) 999-99-99', {
        completed: function () {
            $(this).removeClass('error').addClass('success');
            checkForm($(this).closest('form'));
        }
    });

    // отправить капчу на проверку
    $('.inp-code').mask('9999', {
        completed: function () {
            sendCapcha();
        }
    });

    // проиграть капчу
    $('.js--play-audio').on('click', function () {
        document.getElementById("audiocapcha").play();
        return false;
    });

    // загрузить новую капчу
    $('.js--reload-capcha').on('click', function () {
        loadNewCapcha();
        return false;
    });

    // Отправка почты
    $('.js--form-submit').on('click', function () {
        var form = $(this).closest('form');
        var url = form.attr('action');
        var method = form.attr('method');

        $.ajax({
            type: method,
            url: url,
            data: form.serialize(),
            success: function (data) {
                form.find('.js--form-sended').trigger('click');
            },
            error: function (data) {
                form.find('.error-message').text('Ошибка при отправке');

                setTimeout(function () {
                    form.find('.error-message').text('');
                }, 2000)
            }
        });
    });

    // запрос данных для графика
    $.post('data.json').success(function (data) {
        cheateChart(data);
    }).error(function () {
        $("#chartdiv").hide();
    });
});






